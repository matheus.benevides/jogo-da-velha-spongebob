var jogador = 0;
var jogadas = 0;
var venceu = false;

function mudarJogador() {
	vez = document.getElementById('vez');
	if(jogador == 0)
	{
		jogador = 1;
		vez.className = 'Plankton'
		vez.innerHTML = 'Plankton'
	}else
	{
		jogador = 0;
		vez.className = 'Bob Esponja'
		vez.innerHTML = 'Bob Esponja'
	}
}


function finalizar(){
	tags = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		tags[i].onclick = null;
	}
}

function pegarJogador() {
	if(jogador == 0)
		return 'Bob Esponja'
	else
		return 'Plankton'
}

function iniciar() {
	jogadas = 0;
	venceu = false;
	let casas = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		casas[i].onclick=clique;
		casas[i].style.backgroundImage='';
	}

	mudarJogador()

	document.getElementById('jogar-novamente').style.display = 'none';
	document.getElementById("avisos").innerHTML = 'É A VEZ DE: <span id="vez">'+pegarJogador()+'</span>'
};

function clique(id) {
	console.log("jogador: "+jogador);

	var tag = null;

	if(id.currentTarget)
		tag = id.currentTarget; /* currentTarget é para obter o elemento que foi alvo do evento */
	else
		tag = document.getElementById('casa'+id);

	if((tag.style.backgroundImage == '' || tag.style.backgroundImage == null) && venceu === false)
	{
		var endereco = 'img/'+jogador+'0.jpg';
		tag.style.backgroundImage = 'url('+endereco+')';
		jogadas += 1;

		var ganhou = 0
		if(jogadas >= 5){
			var ganhou = verificarGanhador(tag);

			switch(ganhou)
			{
				case 1:
					if(jogador == 0){
						document.getElementById("avisos").innerHTML = 'Vencedor: <span id="vez">Bob Esponja</span>'
					} else {
						document.getElementById("avisos").innerHTML = 'Vencedor: <span id="vez">Plankton</span>'
					}
					venceu = true;
					document.getElementById('jogar-novamente').style.display = 'block';
					return;
					break;
				case -1:
					document.getElementById("avisos").innerHTML = 'Empate!!!<span id="vez"></span>'
					venceu = true;
					document.getElementById('jogar-novamente').style.display = 'block';
					return;
					break;
				case 0:
					// ninguem ganhou ainda
					break;
			};
		};

		mudarJogador()

		if(ganhou!=0)
		{
			finalizar();
		}
	}
}

function verificarGanhador(jogada){
	const possibilidades = {
    'casa1': [[1, 2, 3], [1, 5, 9], [1, 4, 7]],
    'casa2': [[1, 2, 3], [2, 5, 8]],
    'casa3': [[1, 2, 3], [7, 5, 3], [3, 6, 9]],
    'casa4': [[4, 5, 6], [1, 4, 7]],
    'casa5': [[4, 5, 6], [1, 5, 9], [3, 5, 7], [2, 5, 8]],
    'casa6': [[4, 5, 6], [3, 6, 9]],
    'casa7': [[7, 8, 9], [3, 5, 7], [1, 4, 7]],
    'casa8': [[7, 8, 9], [2, 5, 8]],
    'casa9': [[7, 8, 9], [1, 5, 9], [3, 6, 9]],
  }; // Todas as possibilidades de vitória para cada casa
	let casa = jogada.id;
	var possibilidade = possibilidades[casa];

	for( let pos = 0; pos < possibilidade.length; pos++ ){
		let [casaA, casaB, casaC] = possibilidade[pos];
		let c1 = document.getElementById(`casa${casaA}`).style.backgroundImage;
		let c2 = document.getElementById(`casa${casaB}`).style.backgroundImage;
		let c3 = document.getElementById(`casa${casaC}`).style.backgroundImage;

		if( c1 == c2 && c1 == c3 && c1 != '' ) {
			return 1; // Para 1, se tem o vencedor.
		};
	};

	if( jogadas == 9) {
		return -1;
	};

	return 0;
}
